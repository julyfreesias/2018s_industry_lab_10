package ictgradschool.industry.recursion.ex02;

import java.util.Map;
import java.util.TreeMap;

/**
 * Main program for Lab 10 Ex 2, which should print out a table showing the frequency of all alphanumeric characters
 * in a text block.
 */
public class ExerciseTwo {

    /**
     * Loops through the given String and builds a Map, relating each alphanumeric character in the String (key)
     * with how many times that character occurs in the string (value). Ignore case.
     *
     * @param text the text to analyze
     * @return a mapping between characters and their frequencies in the text
     */
    private Map<Character, Integer> getCharacterFrequencies(String text) {

        // Ignore case. We need only deal with uppercase letters now, after this line.
        text = text.toUpperCase();

        // TODO Initialize the frequencies map to an appropriate implementation class (concrete instance)
        Map<Character, Integer> frequencies = new TreeMap<>();
//        frequencies.put(Character,Integer);

        // Loop through all characters in the given string
        for (char c  : text.toCharArray()) {  //char c-key type and variable name c //

            // If c is alphanumeric...
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {


                // TODO If the map already contains c as a key, increment its value by 1.
                // TODO Otherwise, add it as a new key with the initial value of 1.


                if (frequencies.containsKey(c)){
                    frequencies.put(c,frequencies.get(c)+1);
                }else{
                    frequencies.put(c,1);
                }

            }

        }

        // TODO BONUS: Add any missing keys to the map
        // (i.e. loop through all characters from A-Z and 0-9. If that character doesn't appear in the text,
        // add it as a key here with frequency 0).

        for(char i = 'A'; i<= 'Z'; i++){
            if (!frequencies.containsKey(i)){
                frequencies.put(i,0);
            }
        }
        for(char i='0' ; i <'9' ; i++){
            if (!frequencies.containsKey(i)){
                frequencies.put(i, 0);
            }
        }

        return frequencies;

    }

    /**
     * Prints the given map in a user-friendly table format.
     *
     * @param frequencies the map to print
     */
    private void printFrequencies(Map<Character, Integer> frequencies) {

        System.out.println("Char:\tFrequencies:");
        System.out.println("--------------------");

        // TODO Loop through the entire map and print out all the characters (keys)
        // TODO and their frequencies (values) in a table.
        for(char c : frequencies.keySet()){
            int frequency = frequencies.get(c);
            System.out.println(""+ c + "\t \t \t" + frequency);

            //System.out.println( " Key : " +c + ", Value: " +frequency);
        }



    }

    public String reverse(String s) {
        if(s.length()<=1){
            return s;
        }else{
         return s.charAt(s.length()-1)+reverse(s.substring(0,s.length()-1));
        }
    }

    /**
     * Main program flow. Do not edit.
     */
    private void start() {
        Map<Character, Integer> frequencies = getCharacterFrequencies(Constants.TEXT);
        printFrequencies(frequencies);
        System.out.println(reverse("Hello"));
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
